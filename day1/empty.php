<?php 

	$var = 0;

	// Evaluate to true because $var is empty

	if(empty($var)){

		echo "$var is either 0, empty, or not to set at all<br>";
	}

	//Evaluates as true because var is set

	if (isset($var)) {
		echo "$var is set even though it is empty";
	}

 ?>