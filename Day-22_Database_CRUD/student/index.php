<?php

//db connection

$db = new PDO('mysql:host=localhost;dbname=bitmphp55;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `students` ORDER BY id DESC";

$stmt = $db->query($query);
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>

<div class="container">
    <div class="row">
        <nav>
            <li><a href="create.html">Add new Student</a></li>
        </nav>
        <div class="col-md-offset-3 col-md-6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>SEIP</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php

                foreach($students as $student):

                ?>

                    <tr>
                        <td><?= $student['id'] ?></td>
                        <td><?php echo $student['first_name'] ?></td>
                        <td><?= $student['last_name'] ?></td>
                        <td><?= $student['seip'] ?></td>
                        <td>
                            <a href="show.php?id=<?=$student['id'];?>">Show</a> |
                            <a href="edit.php?id=<?=$student['id'];?>">Edit</a>  |
                            <a href="delete.php?id=<?=$student['id'];?>">Delete</a>
                        </td>
                    </tr>
                <?php
                endforeach;
                ?>

                </tbody>
            </table>

        </div>
    </div>
</div>






<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>