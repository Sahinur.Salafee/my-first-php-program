<?php 

include 'config.php';

/**
* database functionality 
*/
class Database 
{
	private $host   = DB_HOST;
	private $user   = DB_USER; 
	private $pass   = DB_PASS; 
	private $dbname = DB_NAME;

	public $conn;  
	function __construct()
	{
		$this->conn = new Mysqli($this->host, $this->user, $this->pass, $this->dbname);
		if ($this->conn->connect_error) {
			die("Could not connect <br>".$this->conn->connect_error);
		}
	}

	public function DBInsert($name, $email)
	{
		$sql = "INSERT INTO 
				user (name, email)
				VALUES('$name', '$email')";
		if ($this->conn->query($sql)) {
			header("Location: index.php");
			echo "New data inserted successfully";
		} else {
			echo "Failed to insert into database";
		}

	}
}