<?php 
//db connection
$db = new PDO('mysql:host=localhost;dbname=bitmcrud1;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` ORDER BY id DESC";
$stmt = $db->query($query);
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>





<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>index.php</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      .nav1{
        margin-left: 290px;
        padding: 20px;
      }

    </style>
  </head>
  <body>
    
<div class="container">
<div class="row">
<nav class="nav1">
<a href="create.html">Add a new student</a>
</nav>
        <div class="col-md-offset-3 col-md-6">
          <table class="table table-bordered">
          <thead>
            <tr>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>SEIP</th>
              <th>actions</th>
            </tr>
             </thead>
             <tbody>

             <?php 

                  foreach($students as $student){
                  
              ?>

            <tr>
              <td><?= $student['id'] ?></td>
              <td><?= $student['first_name']?></td>
              <td><?= $student['last_name']?></td>
              <td><?= $student['seip']?></td>
              <td>
              <a href="show.php?id=<?= $student['id'] ?>">Show</a> |
              <a href="edit.php">Edit</a> |
              <a href="delete.php?id=<?= $student['id'] ?>">Delete</a>
              </td>
              
            </tr>

            <?php 
                }
             ?>
            </tbody>
         
            
          </table>

        </div>
    </div>
    </div>





    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>